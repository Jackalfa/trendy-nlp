import pandas as pd

#test df
#df = pd.read_csv('https://drive.google.com/u/0/uc?id=18P_e_WGXIJ-RrVK6bDz8FGh6jnwpsl6d&export=download') 
df = pd.read_csv('https://drive.google.com/u/0/uc?id=1S9xC57vIGypktix6qBwNBqYQlJ8NkTHk&export=download  ')
print("Welcome to our hotel recommendation.")
df = df.drop(columns = ['Unnamed: 0'])
df = df.rename(columns={"IdealFor": "IdealforWhom", "IdealStay": "IdealStayDuration", 
"staffPTag": "NiceStaff", "locationPTag": "NiceLocation", "roomPTag": "NiceRoom"
, "staffNTag": "BadStaff", "airNTag": "BadAirCon", "poolNTag": "BadPool"})

colNames = []
bindTogether = {}
for col in df.columns:
    if col != 'Hotel_Name' and col !=  'Hotel_Address' and col !=  'numberOfReviews':
        colNames.append(col)

i=0
notCorrect = 0

print("Chose important factors for your hotel recommendation.")
for col in colNames:
    print(i, " ", colNames[i])
    bindTogether[str(i)] = colNames[i]
    i = i + 1

chosen = []
valuesChosen = input("Simply type numbers like \'1 3 5 11\': ") 
chosen = valuesChosen.strip().split(' ')

chosenFilters = {}
counter = 0
print(chosen)



for number in chosen:
    counter = counter + 1
    attrName = bindTogether[str(number)]
    uniqueList = df[attrName].unique()
    print("Possible values for", attrName, ":")
    for i in range(0,len(uniqueList)):
        print(uniqueList[i])
    print("Choose from options or write 0 if nothing fits you.")
    value = input()

    while notCorrect == 0:
        if (int(value) in uniqueList) or (value == 0) :
            break
        else:
            print("Wrong input. Write something else")
            value = input()

    if value != 0:
        chosenFilters[attrName] = value
        df = df.loc[ df[attrName] == int(chosenFilters[attrName]) ]

    
    numberOfHotels = len(df.index)
    print("So far we can recommend ", numberOfHotels, "hotels like these: ")
    print(df['Hotel_Name', 'Hotel_Address', 'numberOfReviews'].head())

    if counter < len(chosen):
        goFurther = input("Do you wish to continue filtering? yes/no  :")

        if goFurther == 'no' or goFurther == 'No':
            break
        
print("Thank you for using our recommendation system!")